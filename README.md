# Diamond Project's AiiDA Plugin

Plugin developed to make Aiida compatible with the schedulers and machines of interest to workflows developed for the Diamond project (PEPR DIADEM).

## Installation 
This plugin was implemented in agreement with [Aiida Plugins library recomendations](https://aiida.readthedocs.io/projects/aiida-core/en/latest/howto/plugins_develop.html#how-to-plugins-develop-entrypoints), what implies in the use entrypoint to interact directuly with aiida once the package containing the plugin is instaled via `pip`.

To do so, we recomend that the user download this git to its machine (via `git clone` or directly downloading from the GUI on the website). After moving the directory `aiida-diamond` to a adress of choise inside your local computer, use `cd` to navegate via terminal until this folder (in wich `pyproject.toml` should be seen in the output of `ls`). From there, you simply run:

    pip install . -U

The `-U` assures that any previous instalations will be updated to this new one. You should then restart your running AiiDA daemon, in case it exists, via `verdi daemon restart`. You do so cause the deamon of AiiDA only loads the libraries when starting, and you will need it to load the new scheduler. 

If the instalations runned well, the new schedulers should be visible in the output of `verdi plugin list aiida.schedulers`. 

## Usage

* **OAR Scheduler**: While setting up your machines with `verdi computer setup`, include the flag `--scheduler oarscheduler` or add the line `scheduler: "oarscheduler"` to your YAML config file. In addition, while setting a Python script to execute with `verdi run`, the user should inform [`aiida.engine.submit`](https://aiida.readthedocs.io/projects/aiida-core/en/latest/_modules/aiida/engine/runners.html#Runner.submit) with an input that provides the `account` parameter  allowing the scheduler to correct list the project in the OAR script. An example is detailed below :

Setting the field `account` using the `metadata.options` input dictionary of the builder instance of the  `CalcJob` :

```python
builder.metadata.options = {
    "resources": {
        "num_machines": 1,
        ...
    },
    "account": project_name,
    ...
}
```
Then submit the process node :
```python
node = engine.submit(builder)
```
Alternatively, one can also provide a dictionary `inputs = {'metadata':{'options':"account":project_name,...},...}` as a second argument of `engine.submit`.

In the scheduler input file `_aiidasubmit.sh` located in the `RemoteData` node, the project name should now appear in the header :
```bash
#!/bin/bash
...
#OAR --project <my-project-name>
...
```


* **TGCC CCC Scheduler**: Work in progress...
